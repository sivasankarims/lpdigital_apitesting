## LeasePlan Digital- API Automation Project

In this project, I have updated the test scripts <b>to Automate the REST API's in Java</b> using <b>Cucumber</b> as mentioned in the requirments.

The tech stack used for this tutorial are:
1. **JAVA** as the programming language for writing test code
2. **Cucumber** for test cases
3. **TestNg** as the testing framework
4. **Maven** as the build tool
5. **Ecplise** as the preferred IDE for writing java code.

#### Getting Started
Setup your machine.
1. Install JDK (Minium version 1.8)
2. Install Ecplise(Latest One - Ecplise IDE for Developers 2020-06)

#### Cloning & Importing the Project
1. Clone the project from ```https://gitlab.com/sivasankarims/lpdigital_apitesting.git```
2. Import the project (LPDigital_APITesting) in Ecplise ```File -> Import Projects from Git -> Provide the clone URI and proceed ```
3. Now click on ```Finish``` wait until the Ecplise loads project.

#### Downloading & Importing the Project
1. Download the project shared.
2. Import the project (LPDigital_APITesting) in Ecplise ```File -> New -> Project from Existing Sources -> Browse Project Location ```
3. Now click on ```Finish``` wait until the Ecplise loads project.

#### Running tests
1. You can run the tests directly from the Ecplise, by right-clicking on **testng.xml" and **Run As --> TestNg Suite**.

#### Viewing Test Report
1. Go to **test-output** folder in the project.
2. Right click on **emailable-report.html** and select open with **"Web Browser"**
3. You can view the test results of the executed cases.

#### Writing new tests
1. Go to the package **API TestGroup**
2. Select the class file based on GET/PUT/POST/DELETE request.
3. Write a new method based on the API service needed.

#### Updating Test Data
1. Go to the package **utilities**
2. Select the *Constants.java* file
3. Update input data based on service.

---

## Endpoint Interactions

### PetStore - Swagger
I am using [PetStore-Swagger](http://petstore.swagger.io/) for handling the various endpoints.

#### Send a POST Request and validate the Response
1. **[POST Requests](https://gitlab.com/sivasankarims/lpdigital_apitesting/-/blob/master/src/test/java/APITestGroup/POST_PetStore.java):** This class is used for creation of Pet, User and Order

#### Send a GET Request and validate the Response
1. **[GET Requests](https://gitlab.com/sivasankarims/lpdigital_apitesting/-/blob/master/src/test/java/APITestGroup/GET_PetStore.java):** This class is used for fetching details of Pet, User and Order

#### Send a PUT Request and validate the Response
1. **[PUT Requests](https://gitlab.com/sivasankarims/lpdigital_apitesting/-/blob/master/src/test/java/APITestGroup/PUT_PetStore.java):** This class is used for updating details of Pet, User and Order

#### Send a DELETE Request and validate the Response
1. **[DELETE Requests](https://gitlab.com/sivasankarims/lpdigital_apitesting/-/blob/master/src/test/java/APITestGroup/DELETE_PetStore.java):** This class is used for deletion of Pet, User and Order