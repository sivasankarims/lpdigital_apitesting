package builders;

import entities.requests.CreateUserRequest;

public class CreateUserBuilder {

	private CreateUserRequest createUser = new CreateUserRequest();

	public CreateUserBuilder withId(int id) {
		createUser.setId(id);
		return this;
	}

	public CreateUserBuilder withUsername(String username) {
		createUser.setUsername(username);
		return this;
	}

	public CreateUserBuilder withFirstName(String firstName) {
		createUser.setFirstName(firstName);
		return this;
	}

	public CreateUserBuilder withLastName(String lastName) {
		createUser.setLastName(lastName);
		return this;
	}

	public CreateUserBuilder withEmail(String email) {
		createUser.setEmail(email);
		return this;
	}

	public CreateUserBuilder withPassword(String password) {
		createUser.setPassword(password);
		return this;
	}

	public CreateUserBuilder withPhone(String phone) {
		createUser.setPhone(phone);
		return this;
	}

	public CreateUserBuilder withUserStatus(int userStatus) {
		createUser.setUserStatus(userStatus);
		return this;
	}

	public CreateUserRequest build() {
		return createUser;
	}

}
