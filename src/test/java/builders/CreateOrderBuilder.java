package builders;

import entities.requests.CreateOrderRequest;

public class CreateOrderBuilder {

	private CreateOrderRequest orderPetRequest = new CreateOrderRequest();

	public CreateOrderBuilder withId(int id) {
		orderPetRequest.setId(id);
		return this;
	}

	public CreateOrderBuilder withPetId(int petId) {
		orderPetRequest.setPetId(petId);
		return this;
	}

	public CreateOrderBuilder withQuantity(int quantity) {
		orderPetRequest.setQuantity(quantity);
		return this;
	}

	public CreateOrderBuilder withShipDate(String shipDate) {
		orderPetRequest.setShipDate(shipDate);
		return this;
	}

	public CreateOrderBuilder withStatus(String status) {
		orderPetRequest.setStatus(status);
		return this;
	}

	public CreateOrderBuilder withComplete(String complete) {
		orderPetRequest.setComplete(complete);
		return this;
	}

	public CreateOrderRequest build() {
		return orderPetRequest;
	}

}
