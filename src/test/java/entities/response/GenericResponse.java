package entities.response;

public class GenericResponse {

	private int code;
	private String type;
	private String message;

	public int getcode() {
		return code;
	}

	public void setcode(int code) {
		this.code = code;
	}

	public String gettype() {
		return type;
	}

	public void setCategory(String type) {
		this.type = type;
	}

	public String getmessage() {
		return message;
	}

	public void setmessage(String message) {
		this.message = message;
	}

}
