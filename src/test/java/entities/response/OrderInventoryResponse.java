package entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class OrderInventoryResponse {

	private int sold;
	private int pending;
	private int available;
	private int igor1;

	public int getSold() {
		return sold;
	}

	public void setSold(int sold) {
		this.sold = sold;
	}

	public int getPending() {
		return pending;
	}

	public void setPending(int pending) {
		this.pending = pending;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}

	public int getIgor1() {
		return igor1;
	}

	public void setIgor1(int igor1) {
		this.igor1 = igor1;
	}

}
