package APITestGroup;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import entities.response.GenericResponse;
import io.restassured.response.Response;
import utilities.BaseTest;
import utilities.Constants;
import utilities.ResourceHelper;

public class DELETE_PetStore extends BaseTest {
	
	/* Delete a Pet */

	@Test
	public void deleteAPet() throws IOException {
		String deletePetURL = propertiesReader.getEndPointUrl("delete_pet") + Constants.GET_petId;
		Response deletePetResponse = ResourceHelper.delete(deletePetURL);
		GenericResponse deleteResponse = (GenericResponse) getResponseAsObject(deletePetResponse.asString(),
				GenericResponse.class);
		
		Assert.assertEquals(deleteResponse.getcode(), 200);
		
		/* Validate after Pet deletion */
		Response deleteSamePetResponse = ResourceHelper.delete(deletePetURL);
		Assert.assertEquals(deleteSamePetResponse.getStatusCode(), 404);
	}
	
	/* Delete a Purchase order */

	@Test
	public void deletePurchaseOrder() throws IOException {
		String deletePurchaseOrderURL = propertiesReader.getEndPointUrl("delete_purchase_order") + Constants.GET_purchaseId;
		Response deletePurchaseOrderResponse = ResourceHelper.delete(deletePurchaseOrderURL);
		GenericResponse deleteResponse = (GenericResponse) getResponseAsObject(deletePurchaseOrderResponse.asString(),
				GenericResponse.class);
		
		Assert.assertEquals(deleteResponse.getcode(), 200);
		
		/* Validate after Purchase order deletion */
		Response deleteSamePetResponse = ResourceHelper.delete(deletePurchaseOrderURL);
		Assert.assertEquals(deleteSamePetResponse.getStatusCode(), 404);

	}
	
	/* Delete a User */

	@Test
	public void deleteAUser() throws IOException {
		String deleteUserURL = propertiesReader.getEndPointUrl("delete_user") + Constants.POST_addUserName1;
		Response deleteUserResponse = ResourceHelper.delete(deleteUserURL);
		GenericResponse deleteResponse = (GenericResponse) getResponseAsObject(deleteUserResponse.asString(),
				GenericResponse.class);
		
		Assert.assertEquals(deleteResponse.getcode(), 200);
		
		/* Validate after User deletion */
		Response deleteSameUserResponse = ResourceHelper.delete(deleteUserURL);
		Assert.assertEquals(deleteSameUserResponse.getStatusCode(), 404);

	}

}
