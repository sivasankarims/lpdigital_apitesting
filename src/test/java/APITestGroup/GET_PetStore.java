package APITestGroup;

import java.io.IOException;

import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import entities.response.GenericResponse;
import entities.response.OrderInventoryResponse;
import entities.response.OrderPurchaseResponse;
import entities.response.PetStoreResponse;
import entities.response.UserResponse;
import io.restassured.response.Response;
import utilities.BaseTest;
import utilities.Constants;
import utilities.ResourceHelper;

public class GET_PetStore extends BaseTest {
	
	/* Finding Pets by status */

	@Test
	public void fetchAvailablePets() {
		Response availablePetsResponse = ResourceHelper.get(propertiesReader.getEndPointUrl("get_animals_available"));
		AssertJUnit.assertEquals(availablePetsResponse.getStatusCode(), 200);
	}

	@Test
	public void fetchPendingPets() {
		Response pendingPetsResponse = ResourceHelper.get(propertiesReader.getEndPointUrl("get_animals_pending"));
		AssertJUnit.assertEquals(pendingPetsResponse.getStatusCode(), 200);
	}

	@Test
	public void fetchSoldPets() {
		Response soldPetsResponse = ResourceHelper.get(propertiesReader.getEndPointUrl("get_animals_sold"));
		AssertJUnit.assertEquals(soldPetsResponse.getStatusCode(), 200);
	}

	@Test
	public void fetchInvalidPets() {
		Response invalidPetsResponse = ResourceHelper.get(propertiesReader.getEndPointUrl("get_animals_invaild"));
		AssertJUnit.assertEquals(invalidPetsResponse.getStatusCode(), 404);
	}

	/* Finding Pets by ID - Positive scenario */

	@Test
	public void fetchPetDetails() throws IOException {
		String getPetIdURL = propertiesReader.getEndPointUrl("get_pet_by_id") + Constants.GET_petId;// Concatenating the EndPoint & PetId
		Response findSpecificPetResponse = ResourceHelper.get(getPetIdURL);
		PetStoreResponse getPetResponseBasedOnId = (PetStoreResponse) getResponseAsObject(findSpecificPetResponse.asString(),
				PetStoreResponse.class);

		AssertJUnit.assertEquals(findSpecificPetResponse.getStatusCode(), 200);
		Assert.assertEquals(Constants.POST_addPetName, getPetResponseBasedOnId.getName());
	}

	/* Finding Pets by ID - Negative scenario */

	@Test
	public void validatePetDetails() throws IOException {
		String getPetIdURL = propertiesReader.getEndPointUrl("get_pet_by_id") + Constants.GET_petIdNotFound;// Concatenating the EndPoint & PetId
		Response findSpecificPetResponse = ResourceHelper.get(getPetIdURL);
		GenericResponse getPetResponseBasedOnId = (GenericResponse) getResponseAsObject(
				findSpecificPetResponse.asString(), GenericResponse.class);

		Assert.assertEquals("Pet not found", getPetResponseBasedOnId.getmessage());
	}
	
	/* Logs in current logged in user session */

	@Test
	public void loginUser() throws IOException {
		String getLogInUserURL = propertiesReader.getEndPointUrl("get_user_login");
		Response finduserLogin = ResourceHelper.get(getLogInUserURL);

		AssertJUnit.assertEquals(finduserLogin.getStatusCode(), 200);
	}

	/* Logs out current logged in user session */

	@Test
	public void logoutUser() throws IOException {
		String getLogOutUserURL = propertiesReader.getEndPointUrl("get_user_logout");
		Response userLogoutResponse = ResourceHelper.get(getLogOutUserURL);
		GenericResponse getUserLogoutResponse = (GenericResponse) getResponseAsObject(userLogoutResponse.asString(),
				GenericResponse.class);

		AssertJUnit.assertEquals(userLogoutResponse.getStatusCode(), 200);
		Assert.assertEquals("ok", getUserLogoutResponse.getmessage());
	}

	/* Finding User by Username - Positive scenario */

	@Test
	public void fetchUserDetails() throws IOException {
		String getUserURL = propertiesReader.getEndPointUrl("get_user_by_id") + Constants.GET_userName;// Concatenating the EndPoint & UserName
		Response findSpecificUserResponse = ResourceHelper.get(getUserURL);
		UserResponse getPetResponseBasedOnId = (UserResponse) getResponseAsObject(findSpecificUserResponse.asString(),
				UserResponse.class);

		AssertJUnit.assertEquals(findSpecificUserResponse.getStatusCode(), 200);
		Assert.assertEquals(3382, getPetResponseBasedOnId.getId());
	}

	/* Finding User by Username - Negative scenario */

	@Test
	public void validateUserDetails() throws IOException {
		String getUserURL = propertiesReader.getEndPointUrl("get_user_by_id") + Constants.GET_userNameNotFound;// Concatenating the EndPoint & PetId
		Response findSpecificUserResponse = ResourceHelper.get(getUserURL);
		GenericResponse getUserResponseBasedOnUsername = (GenericResponse) getResponseAsObject(
				findSpecificUserResponse.asString(), GenericResponse.class);

		Assert.assertEquals("User not found", getUserResponseBasedOnUsername.getmessage());
	}

	/* Finding purchase order by ID - Positive scenario */

	@Test
	public void fetchOrderDetails() throws IOException {
		String getOrderURL = propertiesReader.getEndPointUrl("get_purchase_by_id") + Constants.GET_purchaseId;// Concatenating the EndPoint & Purchase ID
		Response findSpecificPurchaseResponse = ResourceHelper.get(getOrderURL);
		OrderPurchaseResponse getPurchaseResponseBasedOnId = (OrderPurchaseResponse) getResponseAsObject(
				findSpecificPurchaseResponse.asString(), OrderPurchaseResponse.class);

		AssertJUnit.assertEquals(findSpecificPurchaseResponse.getStatusCode(), 200);
		Assert.assertEquals(Integer.parseInt(Constants.GET_purchaseId), getPurchaseResponseBasedOnId.getPetId());
	}

	/* Finding purchase order by ID - Negative scenario */

	@Test
	public void validateOrderDetails() throws IOException {
		String getOrderURL = propertiesReader.getEndPointUrl("get_purchase_by_id") + Constants.GET_purchaseIdNotFound;// Concatenating the EndPoint & Purchase ID
		Response findSpecificPurchaseResponse = ResourceHelper.get(getOrderURL);
		GenericResponse getPurchaseResponseBasedOnId = (GenericResponse) getResponseAsObject(
				findSpecificPurchaseResponse.asString(), GenericResponse.class);

		Assert.assertEquals("Order not found", getPurchaseResponseBasedOnId.getmessage());
	}
	
	/* Returns pet inventories by status */

	@Test
	public void fetchInventoryDetails() throws IOException {
		String getInventoryURL = propertiesReader.getEndPointUrl("get_pet_inventory");// Concatenating the EndPoint & PetId
		Response findPetInventoryResponse = ResourceHelper.get(getInventoryURL);
		OrderInventoryResponse getPetInventory = (OrderInventoryResponse) getResponseAsObject(
				findPetInventoryResponse.asString(), OrderInventoryResponse.class);

		AssertJUnit.assertEquals(findPetInventoryResponse.getStatusCode(), 200);
		System.out.println("Available Pet count: " + getPetInventory.getAvailable() + " Pending Pet Count: "
				+ getPetInventory.getPending() + " Sold Pet Count: " + getPetInventory.getSold());
	}

}
