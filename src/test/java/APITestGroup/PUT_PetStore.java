package APITestGroup;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import builders.CategoryBuilder;
import builders.CreatePetBuilder;
import builders.CreateUserBuilder;
import builders.TagsBuilder;
import entities.requests.Category;
import entities.requests.CreatePetRequest;
import entities.requests.CreateUserRequest;
import entities.requests.Tags;
import entities.response.GenericResponse;
import entities.response.PetStoreResponse;
import entities.response.UserResponse;
import io.restassured.response.Response;
import utilities.ResourceHelper;
import utilities.BaseTest;
import utilities.Constants;

public class PUT_PetStore extends BaseTest {

	/* Update user by the logged-in user. */

	@Test
	public void updateUserWithLoggedinUser() throws IOException {
		GET_PetStore loggedInUser = new GET_PetStore();
		loggedInUser.loginUser();

		CreateUserRequest updateUserWithLoggedinUser = new CreateUserBuilder()
				.withId(1007)
				.withUsername(Constants.PUT_updatedUserName)
				.withFirstName(Constants.PUT_updatedFirstName)
				.withLastName(Constants.PUT_updatedLastName)
				.withEmail(Constants.PUT_updatedEmail)
				.withPassword(Constants.PUT_updatedPassword)
				.withPhone(Constants.PUT_updatedPhone)
				.withUserStatus(1)
				.build();

		String updateUserURL = propertiesReader.getEndPointUrl("put_update_user") + Constants.PUT_updatedUserName; 
		String updateUserRequestJson = getJsonString(updateUserWithLoggedinUser); 
		Response response = ResourceHelper.update(updateUserURL, updateUserRequestJson);
		GenericResponse updateUserResponse = (GenericResponse) getResponseAsObject(response.asString(), GenericResponse.class);
		
		Assert.assertEquals(updateUserResponse.getcode(), 200);

		String fetchUserURL = propertiesReader.getEndPointUrl("get_user_by_id") + Constants.PUT_updatedUserName;
		Response findSpecificUserResponse = ResourceHelper.get(fetchUserURL);
		UserResponse getPetResponseBasedOnId = (UserResponse) getResponseAsObject(findSpecificUserResponse.asString(),
				UserResponse.class);
		
		Assert.assertEquals(findSpecificUserResponse.getStatusCode(), 200);
		Assert.assertEquals(Constants.PUT_updatedUserName, getPetResponseBasedOnId.getUsername());
		Assert.assertEquals(Constants.PUT_updatedFirstName, getPetResponseBasedOnId.getFirstName());

	}

	/* Update an existing pet */

	@Test
	public void updatePetDetails() throws IOException {
		Category category = new CategoryBuilder()
				.withId(1)
				.withName(Constants.PUT_updatedPetCategoryName)
				.build();

		Tags tags = new TagsBuilder()
				.withId(1)
				.withName(Constants.PUT_updatedPetTagName)
				.build();

		Tags[] tagsList = new Tags[1];
		tagsList[0] = tags;

		String[] photoUrls = { Constants.PUT_updatedPetPhotoURL };

		CreatePetRequest updatePet = new CreatePetBuilder()
				.withCategory(category)
				.withTags(tagsList)
				.withPhotoUrls(photoUrls)
				.withId(1)
				.withName(Constants.PUT_updatedPetName)
				.withStatus(Constants.PUT_updatedPetStatus)
				.build();

		String updatePetURL = propertiesReader.getEndPointUrl("put_update_pet"); 
		String updatePetRequestJson = getJsonString(updatePet); 
		Response response = ResourceHelper.update(updatePetURL, updatePetRequestJson);
		PetStoreResponse updatePetResponse = (PetStoreResponse) getResponseAsObject(response.asString(), PetStoreResponse.class);

		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(updatePet.getId(), updatePetResponse.getId());
		Assert.assertEquals(updatePet.getName(), updatePetResponse.getName());
		Assert.assertEquals(updatePet.getStatus(), updatePetResponse.getStatus());
	}

}
