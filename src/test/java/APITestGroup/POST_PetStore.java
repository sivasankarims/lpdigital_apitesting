package APITestGroup;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import builders.CategoryBuilder;
import builders.CreatePetBuilder;
import builders.CreateUserBuilder;
import builders.CreateOrderBuilder;
import builders.TagsBuilder;
import entities.requests.Category;
import entities.requests.CreatePetRequest;
import entities.requests.CreateUserRequest;
import entities.requests.CreateOrderRequest;
import entities.requests.Tags;
import entities.response.GenericResponse;
import entities.response.PetStoreResponse;
import io.restassured.response.Response;
import utilities.BaseTest;
import utilities.Constants;
import utilities.ResourceHelper;

public class POST_PetStore extends BaseTest {
	
	/* Add a new pet to the store */

	@Test
	public void createPetStore() throws IOException {
		Category category = new CategoryBuilder()
				.withId(1)
				.withName(Constants.POST_addPetCategoryName)
				.build();
		
		Tags tags = new TagsBuilder()
				.withId(1)
				.withName(Constants.POST_addPetTagName)
				.build();

		Tags[] tagsList = new Tags[1];
		tagsList[0] = tags;

		String[] photoUrls = { Constants.POST_addPetPhotoURL };

		CreatePetRequest createPetRequest = new CreatePetBuilder()
				.withId(Integer.parseInt(Constants.GET_petId))
				.withCategory(category)
				.withTags(tagsList)
				.withPhotoUrls(photoUrls)
				.withName(Constants.POST_addPetName)
				.withStatus(Constants.POST_addPetStatus)
				.build();

		String createPetURL = propertiesReader.getEndPointUrl("create_pet");
		String createPetRequestJson = getJsonString(createPetRequest); 
		Response response = ResourceHelper.create(createPetURL, createPetRequestJson);
		PetStoreResponse createPetResponse = (PetStoreResponse) getResponseAsObject(response.asString(), PetStoreResponse.class);

		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(createPetRequest.getName(), createPetResponse.getName());
		Assert.assertEquals(createPetRequest.getStatus(), createPetResponse.getStatus());
		Assert.assertEquals(createPetRequest.getTags()[0].getName(), createPetResponse.getTags()[0].getName());
	}

	/* Place an order for the Pet */

	@Test
	public void createPetOrder() throws IOException {
		CreateOrderRequest orderPetRequest = new CreateOrderBuilder()
				.withId(Integer.parseInt(Constants.GET_purchaseId))
				.withPetId(3382)
				.withQuantity(1)
				.withShipDate(Constants.POST_addPurchaseShipDate)
				.withStatus(Constants.POST_addPurchaseStatus)
				.withComplete(Constants.POST_addPurchaseComplete)
				.build();

		String createOrderURL = propertiesReader.getEndPointUrl("post_pet_order"); 
		String createOrderRequestJson = getJsonString(orderPetRequest);
		Response response = ResourceHelper.create(createOrderURL, createOrderRequestJson);
		CreateOrderRequest petOrderResponse = (CreateOrderRequest) getResponseAsObject(response.asString(),
				CreateOrderRequest.class);

		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(orderPetRequest.getId(), petOrderResponse.getId());
		Assert.assertEquals(orderPetRequest.getPetId(), petOrderResponse.getPetId());
		Assert.assertEquals(orderPetRequest.getQuantity(), orderPetRequest.getQuantity());
		Assert.assertEquals(orderPetRequest.getStatus(), orderPetRequest.getStatus());
	}

	/* Create user with Array */

	@Test
	public void createUserWithArray() throws IOException {
		CreateUserRequest createUser1 = new CreateUserBuilder()
				.withId(104)
				.withUsername(Constants.POST_addUserName1)
				.withFirstName(Constants.POST_addFirstName1)
				.withLastName(Constants.POST_addLastName1)
				.withPassword(Constants.POST_addPassword1)
				.withPhone(Constants.POST_addPhone1)
				.withUserStatus(1)
				.build();
		
		CreateUserRequest createUser2 = new CreateUserBuilder()
				.withId(144)
				.withUsername(Constants.POST_addUserName2)
				.withFirstName(Constants.POST_addFirstName2)
				.withLastName(Constants.POST_addLastName2)
				.withPassword(Constants.POST_addPassword2)
				.withPhone(Constants.POST_addPhone2)
				.withUserStatus(1)
				.build();
		
		CreateUserRequest[] userList = new CreateUserRequest[2];
		userList[0] = createUser1;
		userList[1] = createUser2;

		String createUserArrayURL = propertiesReader.getEndPointUrl("post_create_user_array");
		String createUserRequestJson = getJsonString(userList); 
		Response response = ResourceHelper.create(createUserArrayURL, createUserRequestJson);
		GenericResponse getUserCreationResponse = (GenericResponse) getResponseAsObject(response.asString(),
				GenericResponse.class);

		Assert.assertEquals(getUserCreationResponse.getcode(), 200);
	}

	/* Create user with List */

	@Test
	public void createUserWithList() throws IOException {
		CreateUserRequest createUser1 = new CreateUserBuilder()
				.withId(1009)
				.withUsername(Constants.POST_addUserName3)
				.withFirstName(Constants.POST_addFirstName3)
				.withLastName(Constants.POST_addLastName3)
				.withPassword(Constants.POST_addPassword3)
				.withPhone(Constants.POST_addPhone3)
				.withUserStatus(1)
				.build();
		
		CreateUserRequest createUser2 = new CreateUserBuilder()
				.withId(1008)
				.withUsername(Constants.POST_addUserName4)
				.withFirstName(Constants.POST_addFirstName4)
				.withLastName(Constants.POST_addLastName4)
				.withPassword(Constants.POST_addPassword4)
				.withPhone(Constants.POST_addPhone4)
				.withUserStatus(1)
				.build();

		ArrayList<CreateUserRequest> userList = new ArrayList<CreateUserRequest>();
		userList.add(0, createUser1);
		userList.add(1, createUser2);

		String createUserListURL = propertiesReader.getEndPointUrl("post_create_user_list");
		String createUserRequestJson = getJsonString(userList); 
		Response response = ResourceHelper.create(createUserListURL, createUserRequestJson);
		GenericResponse getUserCreationResponse = (GenericResponse) getResponseAsObject(response.asString(), GenericResponse.class);

		Assert.assertEquals(getUserCreationResponse.getcode(), 200);
	}

	/* Create user by the loggedin user. */

	@Test
	public void createUserWithLoggedinUser() throws IOException {
		GET_PetStore loggedinUser = new GET_PetStore();
		loggedinUser.loginUser();

		CreateUserRequest createUserWithLoggedinUser = new CreateUserBuilder()
				.withId(1007)
				.withUsername(Constants.POST_addUserName5)
				.withFirstName(Constants.POST_addFirstName5)
				.withLastName(Constants.POST_addLastName5)
				.withPassword(Constants.POST_addPassword5)
				.withPhone(Constants.POST_addPhone5)
				.withUserStatus(1)
				.build();

		String createUserURL = propertiesReader.getEndPointUrl("post_create_user_loggedin_user");
		String createUserRequestJson = getJsonString(createUserWithLoggedinUser); 
		Response response = ResourceHelper.create(createUserURL, createUserRequestJson);
		GenericResponse getUserCreationResponse = (GenericResponse) getResponseAsObject(response.asString(), GenericResponse.class);

		Assert.assertEquals(getUserCreationResponse.getcode(), 200);
	}

}
