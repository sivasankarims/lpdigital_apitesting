package utilities;

import io.restassured.response.Response;
import static io.restassured.RestAssured.*;

public class ResourceHelper {

	public static Response get(String endpoint) {
		return given().when().get(endpoint);
	}

	public static Response create(String endpoint, String json) {
		return given().header("Content-Type", "application/json").when().body(json).post(endpoint);
	}

	public static Response update(String endpoint, String json) {
		return given().header("Content-Type", "application/json").when().body(json).put(endpoint);
	}

	public static Response delete(String endpoint) {
		return given().header("Content-Type", "application/json").header("api_key", "special-key").when()
				.delete(endpoint);
	}

}
