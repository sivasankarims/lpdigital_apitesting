package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

	private Properties properties = new Properties();
	private InputStream inputStream = null;

	public PropertiesReader() {
		try {
			String propertiesFilePath = System.getProperty("user.dir") + "/endpoint.properties";
			inputStream = new FileInputStream(propertiesFilePath);
			properties.load(inputStream);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	public String getEndPointUrl(String endpoint) {
		return properties.getProperty("base_url") + properties.getProperty(endpoint);
	}

}
