package utilities;

public class Constants {
	
	/* Update user by the logged-in user. */
	
	public static final String PUT_updatedUserName = "LeasePlan_TestUser17";
	public static final String PUT_updatedFirstName = "LeasePlan_FirstName17";
	public static final String PUT_updatedLastName = "LeasePlan_LastName17";
	public static final String PUT_updatedEmail = "TestUser17@leaseplan.com";
	public static final String PUT_updatedPassword= "xxxxxxx";
	public static final String PUT_updatedPhone = "31623456789";
	
	
	/* Update an existing pet */

	public static final String PUT_updatedPetCategoryName = "Cats";
	public static final String PUT_updatedPetTagName = "Ragdoll";
	public static final String PUT_updatedPetPhotoURL = "https://upload.wikimedia.org/wikipedia/commons/6/64/Ragdoll_from_Gatil_Ragbelas.jpg";
	public static final String PUT_updatedPetName = "Doll";
	public static final String PUT_updatedPetStatus = "Available";
	
	/* Add a new pet to the store */
	
	public static final String POST_addPetCategoryName = "Dogs";
	public static final String POST_addPetTagName = "German Shepherd";
	public static final String POST_addPetPhotoURL = "https://upload.wikimedia.org/wikipedia/commons/d/d0/German_Shepherd_-_DSC_0346_%2810096362833%29.jpg";
	public static final String POST_addPetName = "Hank";
	public static final String POST_addPetStatus = "available";
	
	/* Place an order for the Pet */
	
	public static final String POST_addPurchaseShipDate = "2020-09-20T08:56:54.142Z";
	public static final String POST_addPurchaseStatus = "placed";
	public static final String POST_addPurchaseComplete = "true";

	/* Create user with Array */
	
	public static final String POST_addUserName1 = "LeasePlan_TestUser18";
	public static final String POST_addFirstName1 = "LeasePlan_FirstName18";
	public static final String POST_addLastName1 = "LeasePlan_LastName18";
	public static final String POST_addEmail1 = "TestUser18@leaseplan.com";
	public static final String POST_addPassword1 = "xxxxxxxxx";
	public static final String POST_addPhone1 = "31634567890";
	
	public static final String POST_addUserName2 = "LeasePlan_TestUser19";
	public static final String POST_addFirstName2 = "LeasePlan_FirstName19";
	public static final String POST_addLastName2 = "LeasePlan_LastName19";
	public static final String POST_addEmail2 = "TestUser19@leaseplan.com";
	public static final String POST_addPassword2 = "xxxxxxxxxx";
	public static final String POST_addPhone2 = "31645678901";

	/* Create user with List */
	
	public static final String POST_addUserName3 = "LeasePlan_TestUser20";
	public static final String POST_addFirstName3 = "LeasePlan_FirstName20";
	public static final String POST_addLastName3 = "LeasePlan_LastName20";
	public static final String POST_addEmail3 = "TestUser20@leaseplan.com";
	public static final String POST_addPassword3 = "xxxxxxxxxx";
	public static final String POST_addPhone3 = "31656789012";
	
	public static final String POST_addUserName4 = "LeasePlan_TestUser21";
	public static final String POST_addFirstName4 = "LeasePlan_FirstName21";
	public static final String POST_addLastName4 = "LeasePlan_LastName21";
	public static final String POST_addEmail4 = "TestUser21@leaseplan.com";
	public static final String POST_addPassword4 = "xxxxxxxxxxx";
	public static final String POST_addPhone4 = "31667890123";

	/* Create user by the loggedin user. */
	
	public static final String POST_addUserName5 = "LeasePlan_TestUser22";
	public static final String POST_addFirstName5 = "LeasePlan_FirstName22";
	public static final String POST_addLastName5 = "LeasePlan_LastName22";
	public static final String POST_addEmail5 = "TestUser22@leaseplan.com";
	public static final String POST_addPassword5 = "xxxxxxx";
	public static final String POST_addPhone5 = "31678901234";
	
	/* Finding Pets by ID - Positive scenario */
	
	public static final String GET_petId = "8279";

	/* Finding Pets by ID - Negative scenario */
	
	public static final String GET_petIdNotFound = "500";
	
	/* Finding User by Username - Positive scenario */
	
	public static final String GET_userName = "LeasePlan_TestUser23";

	/* Finding User by Username - Negative scenario */
	
	public static final String GET_userNameNotFound = "test500";

	/* Finding purchase order by ID - Positive scenario */
	
	public static final String GET_purchaseId = "3382";

	/* Finding purchase order by ID - Negative scenario */
	
	public static final String GET_purchaseIdNotFound = "793307";
	
	
	/* Delete a User */
	
	public static final String DELETE_user = "LeasePlan_TestUser17";
	
	/* Delete a Pet */
	
	public static final String DELETE_pet = "LeasePlan_TestUser17";
	
	/* Delete a Order */
	
	public static final String DELETE_order = "LeasePlan_TestUser17";
	
	
	
	
	
}
