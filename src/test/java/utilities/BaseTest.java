package utilities;

import java.io.IOException;

import org.apache.commons.lang3.RandomUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BaseTest {

	protected static PropertiesReader propertiesReader = new PropertiesReader();

	protected int get3DigitRandomInt() {
		return RandomUtils.nextInt(100, 999);
	}

	public static String getJsonString(Object o) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();

		return mapper.writeValueAsString(o);
	}

	public static <T> Object getResponseAsObject(String responseString, Class<T> responseClass) throws IOException {
		ObjectMapper mapper = new ObjectMapper();

		return mapper.readValue(responseString, responseClass);
	}

}
